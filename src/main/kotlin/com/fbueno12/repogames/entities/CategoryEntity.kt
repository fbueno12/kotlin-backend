package com.fbueno12.repogames.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "category")
class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 1L

    @Column(name = "name")
    var name: String = ""

    @ManyToMany(mappedBy = "categories")
    @JsonIgnore
    val games: List<GameEntity> = mutableListOf();

}