package com.fbueno12.repogames.entities

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "game")
class GameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 1

    @Column(name = "name")
    var name: String = ""

    @Column(name = "release_date")
    var releaseDate: LocalDate = LocalDate.now()

    @Column(name = "description")
    var description: String = ""

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(name = "game_company",
        joinColumns = [JoinColumn(name = "company_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "game_id", referencedColumnName = "id")])
    var companies: List<CompanyEntity> = mutableListOf()

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(name = "game_category",
        joinColumns = [JoinColumn(name = "category_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "game_id", referencedColumnName = "id")])
    var categories: List<CategoryEntity> = mutableListOf()
}