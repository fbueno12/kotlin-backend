package com.fbueno12.repogames.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "company")
class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 1L

    @Column(name = "name")
    var name: String = ""

    @Column(name = "description")
    var description: String = ""

    @ManyToMany(mappedBy = "companies")
    @JsonIgnoreProperties("companies")
    val games: List<GameEntity> = mutableListOf();

}