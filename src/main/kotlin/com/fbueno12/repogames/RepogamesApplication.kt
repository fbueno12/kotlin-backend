package com.fbueno12.repogames

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RepogamesApplication

fun main(args: Array<String>) {
	runApplication<RepogamesApplication>(*args)
}
