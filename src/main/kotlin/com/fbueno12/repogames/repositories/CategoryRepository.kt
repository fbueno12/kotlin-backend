package com.fbueno12.repogames.repositories

import com.fbueno12.repogames.entities.CategoryEntity
import org.springframework.data.repository.CrudRepository

interface CategoryRepository: CrudRepository<CategoryEntity, Long>