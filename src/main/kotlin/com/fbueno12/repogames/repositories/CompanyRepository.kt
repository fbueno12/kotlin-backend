package com.fbueno12.repogames.repositories

import com.fbueno12.repogames.entities.CompanyEntity
import org.springframework.data.repository.CrudRepository

interface CompanyRepository: CrudRepository<CompanyEntity, Long>