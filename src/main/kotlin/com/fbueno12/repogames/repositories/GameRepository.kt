package com.fbueno12.repogames.repositories

import com.fbueno12.repogames.entities.GameEntity
import org.springframework.data.repository.CrudRepository

interface GameRepository: CrudRepository<GameEntity, Long>