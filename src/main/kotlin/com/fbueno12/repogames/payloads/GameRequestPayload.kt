package com.fbueno12.repogames.payloads

import com.fasterxml.jackson.annotation.JsonProperty
import com.fbueno12.repogames.entities.GameEntity
import java.time.LocalDate

class GameRequestPayload {

    @JsonProperty("name")
    val name: String = ""

    @JsonProperty("description")
    val descripton: String = ""

    @JsonProperty("release_date")
    val releaseDate: String = ""

    @JsonProperty("categories")
    val categoryId: List<Long> = mutableListOf()

    @JsonProperty("companies")
    val companiesId: List<Long> = mutableListOf()

    fun toEntity(): GameEntity {
        val entity = GameEntity()
        entity.name = this.name
        entity.description = this.descripton
        entity.releaseDate = LocalDate.parse(this.releaseDate)

        return entity
    }

}