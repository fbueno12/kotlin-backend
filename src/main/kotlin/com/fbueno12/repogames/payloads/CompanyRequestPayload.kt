package com.fbueno12.repogames.payloads

import com.fasterxml.jackson.annotation.JsonProperty
import com.fbueno12.repogames.entities.CategoryEntity
import com.fbueno12.repogames.entities.CompanyEntity
import com.fbueno12.repogames.entities.GameEntity

class CompanyRequestPayload {

    @JsonProperty("name")
    val name: String = ""

    @JsonProperty("description")
    val description: String = ""

    fun toEntity(): CompanyEntity {
        val entity = CompanyEntity()
        entity.name = this.name
        entity.description = this.description

        return entity
    }

}