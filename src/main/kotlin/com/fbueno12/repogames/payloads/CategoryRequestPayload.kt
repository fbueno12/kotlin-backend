package com.fbueno12.repogames.payloads

import com.fasterxml.jackson.annotation.JsonProperty
import com.fbueno12.repogames.entities.CategoryEntity
import com.fbueno12.repogames.entities.GameEntity

class CategoryRequestPayload {

    @JsonProperty("name")
    val name: String = ""

    fun toEntity(): CategoryEntity {
        val entity = CategoryEntity()
        entity.name = this.name

        return entity
    }

}