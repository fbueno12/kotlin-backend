package com.fbueno12.repogames.services

import com.fbueno12.repogames.entities.CategoryEntity
import com.fbueno12.repogames.entities.CompanyEntity
import com.fbueno12.repogames.payloads.CompanyRequestPayload
import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.repositories.CompanyRepository
import com.fbueno12.repogames.repositories.GameRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateCompanyService {

    @Autowired
    lateinit var repository: CompanyRepository

    fun execute(payload: CompanyRequestPayload): CompanyEntity {
        val entity: CompanyEntity = payload.toEntity()
        return repository.save(entity)
    }

}