package com.fbueno12.repogames.services

import com.fbueno12.repogames.entities.CategoryEntity
import com.fbueno12.repogames.entities.CompanyEntity
import com.fbueno12.repogames.entities.GameEntity
import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.repositories.CategoryRepository
import com.fbueno12.repogames.repositories.CompanyRepository
import com.fbueno12.repogames.repositories.GameRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateGameService {

    @Autowired
    lateinit var gameRepository: GameRepository

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    @Autowired
    lateinit var companyRepository: CompanyRepository

    fun execute(payload: GameRequestPayload): GameEntity {
        val categories= mutableListOf<CategoryEntity>()
        val companies = mutableListOf<CompanyEntity>()

        payload.categoryId.forEach {
            val category = categoryRepository.findById(it).orElseThrow()
            categories.add(category)
        }
        payload.companiesId.forEach {
            val company = companyRepository.findById(it).orElseThrow()
            companies.add(company)
        }

        val entity: GameEntity = payload.toEntity()
        entity.categories = categories
        entity.companies = companies

        return gameRepository.save(entity);
    }

}