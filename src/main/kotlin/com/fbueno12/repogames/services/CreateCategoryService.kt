package com.fbueno12.repogames.services

import com.fbueno12.repogames.entities.CategoryEntity
import com.fbueno12.repogames.payloads.CategoryRequestPayload
import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.repositories.CategoryRepository
import com.fbueno12.repogames.repositories.GameRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateCategoryService {

    @Autowired
    lateinit var repository: CategoryRepository

    fun execute(payload: CategoryRequestPayload): CategoryEntity {
        val entity: CategoryEntity = payload.toEntity()
        return repository.save(entity)
    }

}