package com.fbueno12.repogames.controllers

import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.services.CreateGameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/games")
class GameController {

    @Autowired
    lateinit var createGameService: CreateGameService

    @PostMapping
    fun save(@RequestBody payload: GameRequestPayload): ResponseEntity<Any> {
        val created = createGameService.execute(payload)
        return ResponseEntity.ok(created)
    }

}