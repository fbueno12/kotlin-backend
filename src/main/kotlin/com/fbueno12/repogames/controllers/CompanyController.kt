package com.fbueno12.repogames.controllers

import com.fbueno12.repogames.payloads.CompanyRequestPayload
import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.services.CreateCompanyService
import com.fbueno12.repogames.services.CreateGameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/companies")
class CompanyController {

    @Autowired
    lateinit var service: CreateCompanyService

    @PostMapping
    fun save(@RequestBody payload: CompanyRequestPayload): ResponseEntity<Any> {
        val created = service.execute(payload)
        return ResponseEntity.ok(created);
    }

}