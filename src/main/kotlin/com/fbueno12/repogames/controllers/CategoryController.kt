package com.fbueno12.repogames.controllers

import com.fbueno12.repogames.payloads.CategoryRequestPayload
import com.fbueno12.repogames.payloads.GameRequestPayload
import com.fbueno12.repogames.services.CreateCategoryService
import com.fbueno12.repogames.services.CreateGameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/categories")
class CategoryController {

    @Autowired
    lateinit var createCategoryService: CreateCategoryService

    @PostMapping
    fun save(@RequestBody payload: CategoryRequestPayload): ResponseEntity<Any> {
        val created = createCategoryService.execute(payload)
        return ResponseEntity.ok().body(created);
    }

}